FROM openjdk:11-jre-slim
EXPOSE 8080
RUN apt-get update && apt install ffmpeg -y
WORKDIR /paopao
COPY ./build/libs/*.jar ./paopao/converter-0.0.1-SNAPSHOT.jar
COPY ./.env.develop ./paopao/.env.develop
COPY ./archive ./paopao/archive
ENTRYPOINT ["java", "-jar", "./paopao/converter-0.0.1-SNAPSHOT.jar"]